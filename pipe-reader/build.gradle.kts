plugins {
    kotlin("multiplatform") version "1.6.21"
}

repositories {
    mavenCentral()
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            dependencies {
                val coroutinesVer = "1.6.1"
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVer")
            }
        }
        binaries {
            executable("pipe_reader") {
                entryPoint = "org.example.pipeReader.main"
                val buildDir = rootProject.project(":pipe-writer").buildDir.absolutePath
                val programFile = "pipe_writer.kexe"
                runTask?.args("$buildDir/bin/linuxX64/pipe_writerDebugExecutable/$programFile")
            }
        }
    }
}
