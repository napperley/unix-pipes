package org.example.pipeReader

import kotlinx.cinterop.*
import kotlinx.coroutines.*
import platform.posix.*
import kotlin.system.exitProcess
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

fun main(args: Array<String>): Unit = runBlocking {
    checkProgramArguments(args)
    val filePath = args.first()
    runServerlessFunction(filePath, "Testing 1234...", ":)")
    runServerlessFunction(filePath, "Testing 1234...", "Message A", "Hello World! :)")
    runServerlessFunction(filePath, "Testing...", "1234", "Message A", "Message B", "End of message.")
}

private suspend fun runProcessTimer(pid: Int, duration: Duration = 3.seconds) = coroutineScope {
    launch {
        delay(duration)
        if (pid > 0 && kill(pid, SIGINT) == 0) println("Process terminated")
    }
}

private fun runForkedProgram(filePath: String, pipeDescriptors: CArrayPointer<IntVar>, vararg args: String): Int {
    val result = fork()
    if (result == 0) {
        // Child process. Let's redirect its standard output to our pipe and replace process with the program.
        close(pipeDescriptors[0])
        dup2(pipeDescriptors[1], STDOUT_FILENO)
        dup2(pipeDescriptors[1], STDERR_FILENO)
        runProgram(filePath, *args)
    }
    // Only the parent process gets here. Intercept the program output.
    close(pipeDescriptors[1])
    return result
}

private fun CoroutineScope.runServerlessFunction(
    filePath: String,
    vararg args: String
) = launch(context = Dispatchers.Default) {
    println("Running Serverless Function...")
    memScoped {
        val pipeDescriptors = allocArray<IntVar>(2)
        pipe(pipeDescriptors)
        val forkPid = runForkedProgram(filePath, pipeDescriptors, *args)
        val output = fdopen(__fd = pipeDescriptors[0], __modes = "r")
        runProcessTimer(forkPid)
        output.readLines(10u).forEach { println(it) }
        // Wait for the child process to terminate.
        val (tmpPid, exitCode) = waitForProcess(forkPid)
        println("Serverless Function (PID: $tmpPid) Exit Code: $exitCode")
    }
}

private fun checkProgramArguments(args: Array<String>) {
    if (args.isEmpty()) {
        printError("Program arguments cannot be empty")
        exitProcess(-1)
    } else if ('/' !in args.first()) {
        printError("First program argument must contain a '/'")
        exitProcess(-1)
    }
}
