package org.example.pipeReader

import kotlinx.cinterop.*
import platform.posix.*

@Suppress("RedundantSuspendModifier")
internal suspend fun CPointer<FILE>?.readLines(limit: UInt = 1u) = memScoped {
    require(limit > 0u) { "The limit parameter must be greater than 0" }
    require(this@readLines != null) { "The receiver cannot be null" }
    val tmp = mutableListOf<String>()
    val line = alloc<CPointerVar<ByteVar>>()
    val size = alloc<ULongVar>()
    var bytesRead: Long
    var pos = 0u
    do {
        bytesRead = getline(__stream = this@readLines, __n = size.ptr, __lineptr = line.ptr)
        if (bytesRead > 0L) {
            tmp += (line.value?.toKString() ?: "").replace("\n", "")
        }
        pos++
    } while (pos < limit)
    tmp.toTypedArray()
}

internal fun runProgram(filePath: String, vararg args: String) {
    when (args.size) {
        1 -> execl(__path = filePath, __arg = filePath, args[0], null)
        2 -> execl(__path = filePath, __arg = filePath, args[0], args[1], null)
        3 -> execl(__path = filePath, __arg = filePath, args[0], args[1], args[2], null)
        4 -> execl(__path = filePath, __arg = filePath, args[0], args[1], args[2], args[3], null)
        5 -> execl(__path = filePath, __arg = filePath, args[0], args[1], args[2], args[3], args[4], null)
        else -> execl(__path = filePath, __arg = filePath, null)
    }
}

internal fun waitForProcess(pid: Int) = memScoped {
    val status = alloc<IntVar>()
    val temp = waitpid(__pid = pid, __stat_loc = status.ptr, __options = 0)
    temp to status.value
}

internal fun printError(msg: String) {
    fprintf(stderr, "$msg\n")
}
