package org.example.pipeWriter

import platform.posix.sleep
import platform.posix.usleep

fun main(args: Array<String>) {
    if (args.isNotEmpty()) args.forEachIndexed { pos, item -> println("Arg ${pos + 1}: $item") }
    println("Ping")
    println("Pong")
    sleep(4u)
    println("Another item...")
    while (true) {
        usleep(500u)
    }
}
