plugins {
    kotlin("multiplatform") version "1.6.21"
}

kotlin {
    linuxX64 {
        binaries {
            executable("pipe_writer") {
                entryPoint = "org.example.pipeWriter.main"
            }
        }
    }
}
